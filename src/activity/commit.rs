use chrono::prelude::*;
use chrono::serde::ts_seconds;

use rusqlite::types::{FromSql, FromSqlResult, ToSql, ToSqlOutput, ValueRef};
use rusqlite::Result;
use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Commit(#[serde(with = "ts_seconds")] DateTime<Utc>);

impl Commit {
    pub fn new(dt: DateTime<Utc>) -> Self {
        Commit(dt)
    }

    pub fn utc(&self) -> DateTime<Utc> {
        self.0
    }

    pub fn local(&self) -> DateTime<Local> {
        self.0.with_timezone(&Local)
    }

    pub fn is_null(&self) -> bool {
        self.0.timestamp() == 0
    }

    // pub fn local_as_string(&self) -> String {
    //     let dt: DateTime<Local> = self.local();
    //     let year = dt.year();
    //     let month = dt.month();
    //     let day = dt.day();
    //     let hour = dt.hour();
    //     let minute = dt.minute();
    //     format!("{year:0>4}-{month:0>2}-{day:0>2} {hour:0>2}:{minute:0>2}")
    // }
}

// impl From<String> for Commit {
//     fn from(commit: String) -> Commit {
//         Commit::from(commit.as_str())
//     }
// }

// impl From<&str> for Commit {
//     fn from(commit: &str) -> Commit {
//         let timestamp: i64 = commit
//             .parse::<i64>()
//             .expect("Problem parsing commit timestamp");
//         let ndt: NaiveDateTime = DateTime::from_timestamp(timestamp, 0)
//             .expect("Problem converting timestamp to NaiveDate");
//         Commit::new(DateTime::from_utc(ndt, Utc))
//     }
// }

impl From<serde_json::Value> for Commit {
    fn from(value: serde_json::Value) -> Commit {
        match value {
            serde_json::Value::String(_) => panic!("Can't convert string to commit"),
            serde_json::Value::Null => panic!("Can't convert null to commit"),
            serde_json::Value::Bool(_) => panic!("Can't convert boolean to commit"),
            serde_json::Value::Number(timestamp) => {
                let utc: DateTime<Utc> = DateTime::from_timestamp(timestamp.as_i64().unwrap(), 0)
                    .expect("Problem converting timestamp to NaiveDate");
                Commit::new(utc)
            }
            serde_json::Value::Array(_) => panic!("Can't convert array to commit"),
            serde_json::Value::Object(_) => panic!("Can't convert object to commit"),
        }
    }
}

impl From<i64> for Commit {
    fn from(timestamp: i64) -> Commit {
        let utc: DateTime<Utc> = DateTime::from_timestamp(timestamp, 0)
            .expect("Problem converting timestamp to NaiveDate");
        Commit::new(utc)
    }
}

impl From<Commit> for String {
    fn from(commit: Commit) -> String {
        commit.0.to_rfc3339()
    }
}

impl FromSql for Commit {
    fn column_result(value: ValueRef) -> FromSqlResult<Self> {
        i64::column_result(value).map(Commit::from)
    }
}

impl ToSql for Commit {
    fn to_sql(&self) -> Result<ToSqlOutput> {
        Ok(self.0.timestamp().into())
    }
}
