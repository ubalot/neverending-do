use std::path::PathBuf;
use structopt::StructOpt;

extern crate neverending_do;

pub mod interactive_mode;
pub mod ndconfig;

use neverending_do as nd;

#[derive(Debug, StructOpt)]
#[structopt(name = "neverending-do", about = "a cli tool for recurrent activities")]
struct Cli {
    /// subcommands
    #[structopt(subcommand)]
    commands: Option<SubCommand>,
}

#[derive(Debug, StructOpt)]
enum SubCommand {
    /// export activity list (as .json file)
    #[structopt(name = "export")]
    Export {
        #[structopt(short = "o", long = "output", parse(from_os_str))]
        path: PathBuf,
    },

    /// import activity list (from .json file)
    #[structopt(name = "import")]
    Import {
        #[structopt(short = "i", long = "input", parse(from_os_str))]
        path: PathBuf,
    },

    /// delete all activities and history (database reset)
    #[structopt(name = "reset")]
    Reset {},
}

fn run_app() -> Result<(), String> {
    let args: Cli = Cli::from_args();
    if let Some(subcommand) = args.commands {
        match subcommand {
            SubCommand::Export { path } => nd::export(path),
            SubCommand::Import { path } => nd::import(path),
            SubCommand::Reset {} => nd::reset(),
        }
    } else {
        interactive_mode::run()
    }
}

fn main() {
    if let Err(e) = ndconfig::load_config() {
        eprintln!("{e:?}");
        std::process::exit(1)
    };

    std::process::exit(match run_app() {
        Ok(_) => 0,
        Err(e) => {
            eprintln!("error: {e:?}");
            2
        }
    });
}
