use rusqlite::{params, Connection, Error, Result, Rows, Statement};

use crate::activity::{category::Category, id::Id, visibility::Visibility, Activity};

const TABLE: &str = "activities";

const COLUMNS: &str = "
    id          INTEGER PRIMARY KEY,
    title       TEXT    NOT NULL,
    category    TEXT    NOT NULL,
    visibility  TEXT    NOT NULL,
    last_commit INT     NOT NULL,
    UNIQUE (title, category, visibility)
    ";

pub struct SQLite {
    conn: Connection,
}

impl SQLite {
    pub fn new(db_name: String) -> Self {
        let conn: Connection = Self::get_connection(db_name).expect("Unable to connect to db.");
        SQLite { conn }
    }

    fn get_connection(db_name: String) -> Result<Connection> {
        let conn: Connection;
        if db_name != "ND_DB_TEST" {
            conn = Connection::open(db_name)?;
            conn.execute(
                format!("CREATE TABLE IF NOT EXISTS {TABLE} ({COLUMNS});").as_str(),
                (),
            )?;
        } else {
            conn = Connection::open_in_memory()?;
            conn.execute(format!("DROP TABLE IF EXISTS {TABLE};").as_str(), ())?;
            conn.execute(format!("CREATE TABLE {TABLE} ({COLUMNS});").as_str(), ())?;
        }

        Ok(conn)
    }

    pub fn reset(&self) -> Result<()> {
        self.conn
            .execute(format!("DROP TABLE IF EXISTS {TABLE};").as_str(), ())?;
        self.conn
            .execute(format!("CREATE TABLE {TABLE} ({COLUMNS});").as_str(), ())?;
        Ok(())
    }

    pub fn get_activities(&self) -> Result<Vec<Activity>> {
        let mut stmt = self
            .conn
            .prepare(format!("SELECT * FROM {TABLE};").as_str())?;
        let activities = stmt.query_map((), |row| {
            let activity = Activity {
                id: Id::new(row.get(0)?),
                title: row.get(1)?,
                category: row.get(2)?,
                visibility: row.get(3)?,
                last_commit: row.get(4)?,
            };
            Ok(activity)
        })?;

        Ok(activities.map_while(|ac| ac.ok()).collect())
    }

    pub fn add(&self, activity: &mut Activity) -> Result<usize> {
        let res: Result<usize, Error> = self.conn.execute(
            format!("INSERT INTO {TABLE} (title, category, visibility, last_commit) VALUES (?1, ?2, ?3, ?4);").as_str(),
            params![
                activity.title,
                activity.category,
                activity.visibility,
                activity.last_commit
            ],
        );

        if res.is_ok() {
            if let Some(ac) = self.find(
                activity.title.clone(),
                activity.category,
                activity.visibility.clone(),
            )? {
                activity.id = ac.id;
            }
        }

        res
    }

    pub fn update(&self, activity: Activity) -> Result<usize> {
        if !activity.id.is_inited() {
            return Err(rusqlite::Error::InvalidQuery);
        }
        self.conn.execute(
            format!("UPDATE {TABLE} SET title = ?1, category = ?2, visibility = ?3, last_commit = ?4 WHERE id = ?5;").as_str(),
            params![activity.title, activity.category, activity.visibility, activity.last_commit, activity.id])
    }

    pub fn find(
        &self,
        title: String,
        category: Category,
        visibility: Visibility,
    ) -> Result<Option<Activity>> {
        let mut stmt: Statement = self.conn.prepare(
            format!(
                "SELECT * FROM {TABLE} WHERE title = ?1 AND category = ?2 AND visibility = ?3;"
            )
            .as_str(),
        )?;
        let mut rows: Rows = stmt.query(params![title, category, visibility])?;

        let mut activities: Vec<Activity> = Vec::new();
        while let Some(row) = rows.next()? {
            let activity = Activity {
                id: row.get(0)?,
                title: row.get(1)?,
                category: row.get(2)?,
                visibility: row.get(3)?,
                last_commit: row.get(4)?,
            };
            activities.push(activity);
        }

        // this check is redundant, uniqueness should be guaranteed by database
        if activities.len() > 1 {
            for activity in activities {
                eprintln!("{activity}");
            }
            panic!("How can it be! More than one activity with same id were found");
        }
        match activities.first() {
            Some(activity) => Ok(Some(activity.clone())),
            None => Ok(None),
        }
    }

    pub fn find_by_id(&self, id: i64) -> Result<Activity> {
        let mut stmt: Statement = self
            .conn
            .prepare(format!("SELECT * FROM {TABLE} WHERE id = ?;").as_str())?;
        let mut rows = stmt.query(params![id])?;

        let mut activities: Vec<Activity> = Vec::new();
        while let Some(row) = rows.next()? {
            let activity = Activity {
                id: row.get(0)?,
                title: row.get(1)?,
                category: row.get(2)?,
                visibility: row.get(3)?,
                last_commit: row.get(4)?,
            };
            activities.push(activity);
        }

        // this check is redundant, uniqueness should be guaranteed by database
        if activities.len() > 1 {
            for activity in activities {
                eprintln!("{activity}");
            }
            panic!("How can it be! More than one activity with same id were found");
        }
        let activity: &Activity = activities.first().expect("No activity found for id {id}");
        Ok(activity.clone())
    }

    pub fn commit(&self, activity: Activity) -> Result<usize> {
        // Insert localtime as last commit date.
        self.conn.execute(
            format!("UPDATE {TABLE} SET last_commit = strftime('%s','now') WHERE id = ?1;")
                .as_str(),
            params![activity.id],
        )
    }

    pub fn remove(&self, activity: Activity) -> Result<usize> {
        self.conn.execute(
            format!("DELETE FROM {TABLE} WHERE id = ?;").as_str(),
            params![activity.id],
        )
    }
}
