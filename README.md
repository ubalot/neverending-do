# Neverending-Do

A simple TODO list for recurrent events (non-recurrent ones are supported, too).

## Dependencies
(For Ubuntu)
```bash
libsqlite3-dev
```

## Usage
[This part will be compiled when an executable will eventually be ready for deployment]

add file `~/.config/neverending-do/config.toml` with content
```bash
db="~/.neverending-do/neverending_do.db"
```


## Development

### Build
```bash
cargo build
```
This step is not necessary as `cargo run` build the project before running it.


### Run clippy
```bash
cargo clippy
```

Apply auto-fix where possible
```bash
cargo clippy --fix
```

### Run tests
```bash
cargo test
```

#### Interactive mode
```bash
cargo run
```

#### Export activities
```bash
cargo run -- export -o backup.json
```

#### Import activities
```bash
cargo run -- import -i backup.json
```

#### Delete all activities
```bash
cargo run -- reset
```
