use configr::{Config, Configr};
use serde::{Deserialize, Serialize};

#[derive(Configr, Deserialize, Serialize, Default)]
pub struct NDConfig {
    db: String,
}

pub fn load_config() -> Result<(), String> {
    let config: NDConfig = match NDConfig::load("neverending-do", true) {
        Ok(c) => c,
        Err(e) => return Err(e.to_string()),
    };

    std::env::set_var("ND_DB", config.db);

    Ok(())
}
